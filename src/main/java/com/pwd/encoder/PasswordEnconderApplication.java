package com.pwd.encoder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PasswordEnconderApplication {

	public static void main(String[] args) {
		SpringApplication.run(PasswordEnconderApplication.class, args);
	}

}
